package com.rmpg.recipes;

public class IngredientList
{
	private Ingredient[] ingredientList;
	
	public IngredientList(String completeString)
	{
		ingredientList = new Ingredient[0];
		for (int nextIng = 0; completeString.indexOf(",") != -1; nextIng++)
		{
			lengthenArray();
			
			String tempName = parseIngName(completeString);
			completeString = advancePastComma(completeString);
			
			double tempNum = parseAmount(completeString);
			completeString = advancePastComma(completeString);
			
			ingredientList[nextIng] = new Ingredient(tempName,tempNum);

		}
	}

	private double parseAmount(String completeString) {
		double tempNum = 0.0;
		if (completeString.indexOf(",") == -1)
			tempNum = Double.parseDouble(completeString);
		else tempNum = Double.parseDouble(parseIngName(completeString));
		return tempNum;
	}

	private String advancePastComma(String completeString) {
		return completeString.substring(completeString.indexOf(",") + 2);
	}

	private String parseIngName(String completeString) {
		return completeString.substring(0,completeString.indexOf(","));
	}
	
	private void lengthenArray()
	{
		Ingredient[] tempArray = new Ingredient[ingredientList.length + 1];
		for (int index = 0; index < ingredientList.length; index++)
			tempArray[index] = ingredientList[index];
		ingredientList = tempArray;
	}
	
	public void print()
	{
		for (int index = 0; index < ingredientList.length; index++)
			ingredientList[index].print();
	}
}
