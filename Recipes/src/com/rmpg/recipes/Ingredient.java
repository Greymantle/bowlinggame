package com.rmpg.recipes;

public class Ingredient
{
	private String nameOfIngredient;
	private double amountOfIngredient;

	public Ingredient (String nameOfIng, double amountOfIng)
	{
		nameOfIngredient = nameOfIng;
		amountOfIngredient = amountOfIng;
	}
	
	public void print()
	{
		System.out.println(amountOfIngredient + " " + nameOfIngredient);
	}
		

}
	
		