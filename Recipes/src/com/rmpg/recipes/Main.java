package com.rmpg.recipes;

public class Main
{ 
	public static void main(String[] args)
	{
		RecipeCard[] cookbook = createCookBook();
		
		cookbook[sortRecipesByCostPerServing(cookbook)].print();
	}
	
	private static RecipeCard[] createCookBook()
	{
		RecipeCard[] cookBook = new RecipeCard[0];
		private String ingStrList = getString();
		for (int counte.isEmpty())
		{
			String nextRecipe = ingListStr.substring(0, ingListStr.indexOf(";"));
			ingListStr = ingListStr.substring(ingListStr.indexOf(";"));
			increaseCookbookSize(cookBook);
			cookBook[cookbook.length - 1] = 
		return cookBook;
	}
	
	private static RecipeCard[] increaseCookbookSize(RecipeCard[] cookBookToBeAddedTo)
	{
		RecipeCard[] tempArray = new RecipeCard[cookBookToBeAddedTo.length + 1];
		for (int index = 0; index < cookBookToBeAddedTo.length; index++)
			tempArray[index] = cookBookToBeAddedTo[index];
		cookBookToBeAddedTo = tempArray;
		return cookBookToBeAddedTo;
	}

	private static RecipeCard[] addToCookBook(RecipeCard[] cookBookToBeAddedTo, String someRecipe)
	{
		RecipeCard[] tempArray = new RecipeCard[cookBookToBeAddedTo.length + 1];
		for (int index = 0; index < cookBookToBeAddedTo.length; index++)
			tempArray[index] = cookBookToBeAddedTo[index];
		cookBookToBeAddedTo = tempArray;
		RecipeCard any = new RecipeCard(someRecipe);
		cookBookToBeAddedTo[tempArray.length - 1] = any;
		return cookBookToBeAddedTo;
	}	


	private static int sortRecipesByCostPerServing(RecipeCard[] unsortedRecipeCards)
	{
		int numberOfCard = 0;
		for (numberOfCard = 0; numberOfCard < unsortedRecipeCards.length - 1; numberOfCard++)
		{
			for (int nextCard = numberOfCard + 1; nextCard < unsortedRecipeCards.length - 3; nextCard++)
			{
				if (unsortedRecipeCards[nextCard] == null) return numberOfCard;
				else if ((unsortedRecipeCards[nextCard].getCost() / unsortedRecipeCards[nextCard].getNumOfServings()) <
						(unsortedRecipeCards[numberOfCard].getCost() / unsortedRecipeCards[numberOfCard].getNumOfServings()))
						numberOfCard = nextCard;
			}
		}
		return numberOfCard;
	}

}
