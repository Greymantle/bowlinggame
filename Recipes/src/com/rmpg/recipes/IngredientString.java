package com.rmpg.recipes;

public class IngredientString 
{
	private String ingString = "Cereal, true, 1, 180, 1.8, cup of cereal, 1.0, cup of milk, 1.0; +" +
			"Waffles, true, 3, 540, 1.2, cup of flour, 1, cup of milk, 1, number of eggs, 1, tsp of baking soda, 1; +" +
			"ScrambledEggs, true, 1, 80, .27, number of eggs, 1, tsp of milk, 1";
	
	public String getString()
	{
		return ingString;
	}
}
