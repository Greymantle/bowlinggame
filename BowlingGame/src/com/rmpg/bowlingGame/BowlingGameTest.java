package com.rmpg.bowlingGame;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

public class BowlingGameTest 
{
//game: 5/4,6/4,6/3,7/3,6/4,10,6/3,10,10,8/2/10
//totals:9, 25, 34, 50, 70, 89,98,126,146,166
    @Ignore
    @Test
	public void testOneFrame() 
	{
	    int[] ballsThrown = {5,4};
		int gameScore = setUpATestGame(ballsThrown);
		assertEquals(9, gameScore);
	}

private int setUpATestGame(int[] ballsThrown)
{
    BowlingGame aTestGame = new BowlingGame();
    int gameScore = aTestGame.gameScore(ballsThrown);
    return gameScore;
}

    @Ignore
    @Test
	public void  testTwoFrames()
	{
	    int[] ballsThrown = {5,4,6,2};
		int gameScore = setUpATestGame(ballsThrown);
		assertEquals(17, gameScore);		
	}
	
	@Test
	@Ignore
	public void testSpare()
	{
	    int[] ballsThrown = {5,5,6,3};
	    int gameScore = setUpATestGame(ballsThrown);
	    assertEquals(25, gameScore);
	}
	
	@Test
	@Ignore
	public void testStrike()
	{
	    int[] ballsThrown = {10,6,3};
	    int gameScore = setUpATestGame(ballsThrown);
        assertEquals(28, gameScore);
	}
	
	@Test
	public void testNoStrikeGame()
	{
	    int[] ballsThrown = {4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4};
        int gameScore = setUpATestGame(ballsThrown);
        assertEquals(80, gameScore); 
	}
	
	@Test
	public void testAllSpares()
	{
	    int[] ballsThrown = {5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5};
        int gameScore = setUpATestGame(ballsThrown);
        assertEquals(150, gameScore); 
	}
	
	@Test
	public void testAllStrikes()
	{
	    int[] ballsThrown = {10,10,10,10,10,10,10,10,10,10,10,10};
        int gameScore = setUpATestGame(ballsThrown);
        assertEquals(300, gameScore);
	}
	 @Test
	 public void testMixedGame()
	 {
	     int[] ballsThrown = {5,4,6,4,6,3,7,3,6,4,10,6,3,10,10,8,2,10};
	     int gameScore = setUpATestGame(ballsThrown);
	        assertEquals(166, gameScore);
	 }
}